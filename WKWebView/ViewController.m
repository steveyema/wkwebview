//
//  ViewController.m
//  WKWebView
//
//  Created by Ye Ma on 2016-08-11.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>
#import "WebViewController.h"

@interface ViewController ()

@property(nonatomic, strong) WebViewController *controller;
- (IBAction)layoutPressed:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:config];
    [webView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    _controller = [WebViewController instanceWithWebView:webView];

    [self.view addSubview:webView];

    id views = NSDictionaryOfVariableBindings(webView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[webView]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[webView]|" options:0 metrics:nil views:views]];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.google.com"]]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)layoutPressed:(id)sender {
    
    [self.controller handleLayoutPressed:sender];
}
@end
