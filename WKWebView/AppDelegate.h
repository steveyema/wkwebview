//
//  AppDelegate.h
//  WKWebView
//
//  Created by Ye Ma on 2016-08-11.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

