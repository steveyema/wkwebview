//
//  WebViewController.h
//  WKWebView
//
//  Created by Ye Ma on 2016-08-11.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface WebViewController : NSObject

+(instancetype)instanceWithWebView:(WKWebView *)webView;
- (void)handleLayoutPressed:(id)sender;

@end
