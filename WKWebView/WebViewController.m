//
//  WebViewController.m
//  WKWebView
//
//  Created by Ye Ma on 2016-08-11.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "WebViewController.h"

const NSInteger CTA_PAGE_TAG = 999;

@interface WebViewController()<WKNavigationDelegate, NSURLConnectionDelegate>
{
    NSMutableDictionary *_dataDictionary;
    NSMutableDictionary *_requestDictionary;
    NSMutableDictionary *_webViewDictionary;
}

@property(nonatomic, strong) WKWebView *_webView;
@property(nonatomic, strong) UIView *ctaView;

@end

@implementation WebViewController

+(instancetype)instanceWithWebView:(WKWebView *)webView
{
    WebViewController *controller = [[WebViewController alloc] init];
    controller._webView = webView;
    webView.navigationDelegate = controller;
    
    return controller;
}

- (instancetype)init
{
    if (self = [super init])
    {
        _dataDictionary = [[NSMutableDictionary alloc] init];
        _requestDictionary = [[NSMutableDictionary alloc] init];
        _webViewDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)handleLayoutPressed:(id)sender
{

}

- (UIView *)findWebBrowsingView:(UIScrollView *)scrollView
{
    UIView *webBrowsingView = nil;
    for (UIView *view in scrollView.subviews)
    {
        if ([NSStringFromClass(view.class) isEqualToString:@"WKContentView"])
        {
            webBrowsingView = view;
            break;
        }
    }
    return webBrowsingView;
}

- (void)loadCTAPage:(UIScrollView *)scrollView
{
    UIView *view = [scrollView viewWithTag:CTA_PAGE_TAG];
    
    if (!view)
    {
        UIView *contentView = [self findWebBrowsingView:scrollView];
        
        self.ctaView = [[UIView alloc] initWithFrame:scrollView.frame];
        self.ctaView.backgroundColor = [UIColor redColor];
        self.ctaView.tag = CTA_PAGE_TAG;
        
        [self.ctaView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [scrollView addSubview:self.ctaView];
        
        CGFloat initialCTAHeight = CGRectGetHeight(self.ctaView.frame);
        NSLayoutConstraint *CTAHeight = [NSLayoutConstraint constraintWithItem:self.ctaView
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1
                                                                      constant:initialCTAHeight];
        [self.ctaView addConstraint:CTAHeight];
        
        NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:scrollView
                                                                            attribute:NSLayoutAttributeBottom
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.ctaView
                                                                            attribute:NSLayoutAttributeBottom
                                                                           multiplier:1
                                                                             constant:0];
        [scrollView addConstraint:bottomConstraint];
        
        NSDictionary *views = @{ @"cta" : self.ctaView, @"contentView" : contentView };
        [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cta]|" options:0 metrics:nil views:views]];
        //[scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[contentView]-0-[cta]|" options:0 metrics:nil views:views]];
        [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:contentView
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.ctaView
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0]];
        
        [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:scrollView
                                                               attribute:NSLayoutAttributeCenterX
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.ctaView
                                                               attribute:NSLayoutAttributeCenterX
                                                              multiplier:1
                                                                constant:0]];
        
    }
    
    [scrollView setNeedsLayout];
    [scrollView layoutIfNeeded];

}

#pragma  mark - WKNavigationDelegate methods
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if ([_webViewDictionary objectForKey:navigationAction.request.URL])
    {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    
    [_webViewDictionary setObject:webView forKey:navigationAction.request.URL];
  
    [NSURLConnection connectionWithRequest:navigationAction.request delegate:self];
    
    decisionHandler(WKNavigationActionPolicyCancel);

//    [webView loadHTMLString:@"" baseURL:navigationAction.request.URL];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [self loadCTAPage:webView.scrollView];
}

#pragma mark - NSURLConnection Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSValue *key = [NSValue valueWithNonretainedObject:connection];
    NSMutableData *storedData = [_dataDictionary objectForKey:key];
    if (storedData)
    {
        [storedData appendData:data];
    }
    else
    {
        [_dataDictionary setObject:[data mutableCopy] forKey:key];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSValue *key = [NSValue valueWithNonretainedObject:connection];
    [_requestDictionary setObject:response.URL forKey:key];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self clearNetworkDictionaries:connection];
    
//    UIWebView *webView = [_webViewDictionary objectForKey:connection.originalRequest.URL];
//    [self.pageViewController webViewConnectionError:(UIWebView *)webView];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSValue *key = [NSValue valueWithNonretainedObject:connection];
    NSMutableData *storedData = [_dataDictionary objectForKey:key];
    if (storedData)
    {
        NSString *content = [[NSString alloc] initWithData:storedData encoding:NSUTF8StringEncoding];
        if (!content)
        {
            content = [[NSString alloc] initWithData:storedData encoding:NSASCIIStringEncoding];
        }
        
        if (content)
        {
            // Get the right webview that was stored from the original request.
            UIWebView *webView = [_webViewDictionary objectForKey:connection.originalRequest.URL];
            
            // Manually load the content in our webview.
            // Get the appropriate URL for the request... possibly has changed from redirects.
            NSURL *url = [_requestDictionary objectForKey:key];
            
            [self loadWebview:webView withHTML:content baseURL:url];
            [self clearNetworkDictionaries:connection];
        }
    }
}

- (void)loadWebview:(UIWebView *)webView withHTML:(NSString *)html baseURL:(NSURL *)baseURL
{
    NSMutableString *mutableHTML = [NSMutableString stringWithString:html];

    
    // Inserting our updated meta tag and js into the correct locations
    NSRange rangeOfHead = [html rangeOfString:@"</body>"];
    if (rangeOfHead.location != NSNotFound)
    {
        NSString *metaWidth = [NSString stringWithFormat:@"<div style='height:%fpx;'/>", webView.frame.size.height];
        // Only insert the script if we can have a valid range for head tag
        NSInteger insertIndex = rangeOfHead.location;
        [mutableHTML insertString:metaWidth atIndex:insertIndex];
        insertIndex += metaWidth.length;
    }
    
    [webView loadHTMLString:mutableHTML baseURL:baseURL];
}

- (void)clearNetworkDictionaries:(NSURLConnection *)connection
{
    NSValue *key = [NSValue valueWithNonretainedObject:connection];
    [_dataDictionary removeObjectForKey:key];
    [_requestDictionary removeObjectForKey:key];
    [_requestDictionary removeObjectForKey:connection.originalRequest.URL];
}


@end
